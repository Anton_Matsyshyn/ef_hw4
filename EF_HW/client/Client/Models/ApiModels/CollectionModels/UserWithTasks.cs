﻿using System.Collections.Generic;

namespace Client.Models.ApiModels.CollectionModels
{
    public class UserWithTasks
    {
        public User User { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
    }
}
