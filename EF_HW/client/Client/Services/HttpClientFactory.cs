﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Client.Services
{
    public class HttpClientFactory
    {
        readonly IConfigurationRoot _configuration;

        public HttpClientFactory(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        public HttpClient GetClient()
        {
            var netSettings = _configuration.GetSection("NetSettings");

            var host = netSettings.GetSection("Host").Value;
            var port = netSettings.GetSection("Port").Value;
            var apiEndpoint = netSettings.GetSection("ApiEndpoint").Value;

            return new HttpClient() { BaseAddress=new Uri($"{host}:{port}/{apiEndpoint}/")};
        }
    }
}
