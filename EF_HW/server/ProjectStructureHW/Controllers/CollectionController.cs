﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProjectStructureHW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionController : ControllerBase
    {
        private readonly ICollectionService _collectionService;

        public CollectionController(ICollectionService collectionService)
        {
            _collectionService = collectionService;
        }

        /// <summary>
        /// This is solution for 1st task from LINQ homework
        /// </summary>
        [HttpGet("ProjectIdAndTaskCount/{projectOwnerId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetProjectsIdAndTaskCount(int projectOwnerId)
        {
            try
            {
                return new JsonResult(_collectionService.GetProjectsIdAndTaskCount(projectOwnerId)
                                                        .Select(o => new { Key = o.Key, Value = o.Value }));
            }
            catch(NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This is solution for 2nd task from LINQ homework
        /// </summary>
        [HttpGet("UsersTask/{userId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetUsersTask(int userId)
        {
            try
            {
                return new JsonResult(_collectionService.GetUsersTask(userId));
            }
            catch(NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This is solution for 3d task from LINQ homework
        /// </summary>
        [HttpGet("FinishedTask/{userId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetFinishedUserTasks(int userId)
        {
            try
            {
                return new JsonResult(_collectionService.GetFinishedUserTasks(userId)
                                                        .Select(x => new { TaskId = x.Id, TaskName = x.Name }));
            }
            catch(NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This is solution for 4th task from LINQ homework
        /// </summary>
        [HttpGet("TeamsWithUsers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetTeamsWithUsers()
        {
            try
            {
                return new JsonResult(_collectionService.GetTeamsWithUsers());
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This is solution for 5th task from LINQ homework
        /// </summary>
        [HttpGet("UsersWithTasks")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetUsersWithTasks()
        {
            try
            {
                return new JsonResult(_collectionService.GetUsersWithTasks());
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This is solution for 6th task from LINQ homework
        /// </summary>
        [HttpGet("LastUserProject/{userId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetLastUserProject(int userId)
        {
            try
            {
                return new JsonResult(_collectionService.GetLastUserProject(userId));
            }
            catch(NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This is solution for 7th task from LINQ homework
        /// </summary>
        [HttpGet("ProjectWithTeam")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetProjectWithTeam()
        {
            try
            {
                return new JsonResult(_collectionService.GetProjectWithTeam());
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
