﻿using DataAccessLayer.Interfaces;
using System.Threading.Tasks;

namespace DataAccessLayer.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly HomeworkDbContext _context;

        public UnitOfWork(HomeworkDbContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
