﻿using DataAccessLayer.Interfaces;
using DataAccessLayer.Models.Abstractions;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DataAccessLayer.Services
{
    public class Repository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly HomeworkDbContext _context;
        public Repository(HomeworkDbContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            this.unitOfWork = unitOfWork;
        }

        public IUnitOfWork unitOfWork { get; }

        public void Create(T entity)
        {
            entity.Id = 0;
            _context.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = _context.Set<T>().Find(id);
            _context.Remove(entity);
        }

        public IQueryable<T> Get()
        {
            return _context.Set<T>().AsQueryable();
        }

        public T Get(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public void Update(T updatedEntity)
        {
            _context.Entry(updatedEntity).State = EntityState.Modified;
        }
    }
}
