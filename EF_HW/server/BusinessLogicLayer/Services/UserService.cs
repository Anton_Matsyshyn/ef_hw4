﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IRepository<User> _repository;

        public UserService(IRepository<User> repository,
                           IMapper mapper) : base(mapper)
        {
            _repository = repository;
        }
        public int CreateUser(UserDTO userDto)
        {
            if (userDto == null)
                throw new NullEntityException(typeof(UserDTO));

            var user = _mapper.Map<User>(userDto);

            _repository.Create(user);
            _repository.unitOfWork.SaveChanges();

            userDto.Id = user.Id;

            return userDto.Id;
        }

        public void DeleteUser(int userId)
        {
            if (GetUser(userId) == null)
                throw new NotFoundException(typeof(User), userId);

            _repository.Delete(userId);
            _repository.unitOfWork.SaveChanges();
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _repository.Get().Select(u => _mapper.Map<UserDTO>(u));
        }

        public UserDTO GetUser(int userId)
        {
            User entity = _repository.Get(userId);

            if (entity == null)
                throw new NotFoundException(typeof(User), userId);

            var dto = _mapper.Map<UserDTO>(entity);
            return dto;
        }

        public void UpdateUser(UserDTO userDto)
        {
            if (userDto == null)
                throw new NullEntityException(typeof(UserDTO));

            if (GetUser(userDto.Id) == null)
                throw new NotFoundException(typeof(User), userDto.Id);

            var userEntity = _repository.Get(userDto.Id);
            userEntity = _mapper.Map(userDto, userEntity);

            _repository.Update(userEntity);
            _repository.unitOfWork.SaveChanges();
        }
    }
}
