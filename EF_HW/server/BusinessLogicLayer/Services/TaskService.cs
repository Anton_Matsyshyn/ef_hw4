﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly IRepository<Task> _repository;
        public TaskService(IRepository<Task> repository,
                           IMapper mapper) : base(mapper)
        {
            _repository = repository;
        }

        public int CreateTask(TaskDTO taskDto)
        {
            if (taskDto == null)
                throw new NullEntityException(typeof(TaskDTO));

            var task = _mapper.Map<Task>(taskDto);

            _repository.Create(task);
            _repository.unitOfWork.SaveChanges();

            taskDto.Id = task.Id;

            return taskDto.Id;
        }

        public void DeleteTask(int taskId)
        {
            if (GetTask(taskId) == null)
                throw new NotFoundException(typeof(Task), taskId);

            _repository.Delete(taskId);

            _repository.unitOfWork.SaveChanges();
        }

        public IEnumerable<TaskDTO> GetAllTask()
        {
            return _repository.Get().Select(t => _mapper.Map<TaskDTO>(t));
        }

        public TaskDTO GetTask(int taskId)
        {
            Task entity = _repository.Get(taskId);

            if (entity == null)
                throw new NotFoundException(typeof(Task), taskId);

            var dto = _mapper.Map<TaskDTO>(entity);
            return dto;
        }

        public void UpdateTask(TaskDTO task)
        {
            if (task == null)
                throw new NullEntityException(typeof(TaskDTO));

            if (GetTask(task.Id) == null)
                throw new NotFoundException(typeof(Task), task.Id);

            var taskEntity = _repository.Get(task.Id);
            taskEntity = _mapper.Map(task, taskEntity);

            _repository.Update(taskEntity);
            _repository.unitOfWork.SaveChanges();
        }
    }
}
