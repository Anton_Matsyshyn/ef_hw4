﻿using Common.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITaskStateService
    {
        IEnumerable<TaskStateDTO> GetTaskStates();
    }
}
