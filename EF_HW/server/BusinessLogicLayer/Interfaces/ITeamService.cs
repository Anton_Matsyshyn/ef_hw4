﻿using Common.DTO;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetAllTeams();
        TeamDTO GetTeam(int teamId);

        int CreateTeam(TeamDTO team);
        void UpdateTeam(TeamDTO team);
        void DeleteTeam(int teamId);
    }
}
